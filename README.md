# Counter-Strike 1.6 Source

This is a source code of the straight port of classic Counter-Strike 1.6 to the Source Engine. The original author of the source code is Aulov Nikolay Georgievich. Feel free to use the source code to make your own mods.